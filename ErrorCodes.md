Error Code  | Error Message    | Relevant Resources  | Parameters
----------- | ----------|------------ |-----
1001  | Invalid resource name {0} given  | All Resources  | `0 - Resource Name`
1002 | Internal server error | All Resources | None
1003 | Missing required parameter {0} | All Resources | `0 -  Property or Value Name`
1004 | Given {0} does not exist | All Resources | None |`0 -  Property Name`
1005 | The {0} cannot be updated | All Resources| None | `0 -  Property Name`
1006 | The {0} cannot be deleted | All Resources | None | `0 -  Property Name`
1007 | Misssing token | All Resources | None | 
1008 | Invalid decrypted Token | All Resources | None | 
1009 | Invalid Token | All Resources | None | 
1010 | Expired Token | All Resources | None | 
1011 | Username required for token generation | All Resources | None | 
1012 | Password required for token generation | All Resources | None | 
1013 | Invalid username or password| All Resources | None | 
