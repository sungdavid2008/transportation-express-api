# README #

To Setup the Environment for the first time:
```
#!javascript

npm install
```
To Setup the Intellisense in VisualStudio Code:
```
#!javascript

typings install
```
To Run Tests:
```
#!javascript

./node_modules/.bin/mocha -u exports tests
```
Token required to access APP:
```
#!javascript
VTJGc2RHVmtYMTg1Zk1LdVJHd3Q5Y2svcjJnUTA5bWFBSEhkYTMxSEVMc255WWgwUk5ZM0RidHFHVTFDblFoeEI0U2NGU01nNW9hVlFMQ2l5ZGZuUUJDd2EzWmUxenZTblBkakh5Q3ZKMWM9
```
### What is this repository for? ###

* Quick summary: Code Example for APP Class
* Version
0.0.2
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)