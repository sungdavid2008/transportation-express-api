/** 
 * Example of RESTful API using Express and NodeJS
 * @author Clark Jeria, David Sung
 * @version 0.0.2
 */

/** BEGIN: Express Server Configuration */
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var CryptoJS = require("crypto-js");
var base64 = require("js-base64").Base64;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;

var mongoose = require('mongoose');
mongoose.connect('mongodb://ysung:oct1031@ds041506.mlab.com:41506/cmu_app');
/** END: Express Server Configuration */

/** BEGIN: Express Routes Definition */
var router = require('./routes/router');
var cars = require('./routes/cars');
var drivers = require('./routes/drivers');
var passengers = require('./routes/passengers');
var paymentAccounts = require('./routes/paymentaccounts');
var rides = require('./routes/rides');
var sessions = require('./routes/sessions');

/*Check token when login into APP*/
app.use(function (req, res, next) {
    headers = req.headers;


    if (req.url != '/sessions') {
        /*Misssing token*/
        if (headers.token === undefined) {
            res.status(400).json({ "errorCode": "1007", "errorMessage": "Misssing token", "statusCode": "400" });
            return;
        } else {
            /*Decrypt token*/
            cryptedHash = base64.decode(headers.token);
            uncryptedHash = CryptoJS.AES.decrypt(cryptedHash, "Secret").toString(CryptoJS.enc.Utf8);

            try {
                username = uncryptedHash.split(':')[0];
                expiration = uncryptedHash.split(':')[1];
                HashedClearString = uncryptedHash.split(":")[2];

                clearString = username + ":" + expiration;
                reHashedClearString = CryptoJS.HmacSHA1(clearString, "APP");
                /*Compare with the encryption in database*/
                if (HashedClearString != reHashedClearString) {
                    res.status(400).json({ "errorCode": "1008", "errorMessage": "Invalid decrypted Token", "statusCode": "400" });
                    return;
                }
            } catch (error) {
                res.status(400).json({ "errorCode": "1009", "errorMessage": "Invalid Token", "statusCode": "400" });
                return;
            }
            /*Check to see if the token has been expired*/
            if (expiration < parseInt(Date.now() / 1000)) {
                res.status(400).json({ "errorCode": "1010", "errorMessage": "Expired Token", "statusCode": "400" });
                return;
            }
        }
    }
    next();
});

app.use('/api', cars);
app.use('/api', drivers);
app.use('/api', passengers);
app.use('/api', paymentAccounts);
app.use('/api', rides);
app.use('/api', router);
app.use('/api', sessions);


/** END: Express Routes Definition */

/** BEGIN: Express Server Start */
app.listen(port);
console.log('Service running on port ' + port);

module.exports = app;
/** END: Express Server Start */