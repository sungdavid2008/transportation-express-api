/** 
 * Express Route: /sessions
 * @author David sung
 * @version 0.0.3
 */
var express = require('express');
var router = express.Router();
var util = require('util');
var mongoose = require('mongoose');

var CryptoJS = require("crypto-js");
var base64 = require("js-base64").Base64;



router.route('/sessions')

    .post(function (req, res) {
        /*defined default username and password*/
        const usernameref = "john";
        const passwordref = "1234"
        const hashPasswordref = CryptoJS.HmacSHA1(passwordref, "pw").toString();
        /*receive username and password*/
        var username = req.body.username;
        var password = req.body.password;

        /*No username to generate token*/
        if (req.body.username == undefined) {
            res.status(400).json({ "errorCode": 1011, "errorMsg": "Username required for token generation", "statusCode": 400 });
            return;
        }
        /*No password to generate token*/
        if (req.body.password == undefined) {
            res.status(400).json({ "errorCode": 1012, "errorMsg": "Password required for token generation", "statusCode": 400 });
            return;
        }

        /*Username is not equal to default username*/
        if (req.body.username != usernameref) {
            res.status(400).json({ "errorCode": 1013, "errorMsg": "Invalid username or password", "statusCode": 400 });
            return;
        }

        /*Password is not equal to default password*/
        var hashPassword = CryptoJS.HmacSHA1(req.body.password, "pw").toString();
        if (hashPassword != hashPasswordref) {
            res.status(400).json({ "errorCode": 1013, "errorMsg": "Invalid username or password", "statusCode": 400 });
            return;
        }

        /*Encryption*/
        expiration = (parseInt(Date.now() / 1000) + 60 * 60 * 24 * 365 * 2);
        clearString = username + ":" + expiration;
        hashString = CryptoJS.HmacSHA1(clearString, "APP");
        cryptString = CryptoJS.AES.encrypt(clearString + ":" + hashString, "Secret").toString();
        response = { token: base64.encode(cryptString) }
        console.log("token", response)
        res.status(200).json(response);
    });

module.exports = router;




