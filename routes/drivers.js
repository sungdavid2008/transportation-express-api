/** 
 * Express Route: /drivers
 * @author Clark Jeria, David Sung
 * @version 0.0.3
 */
var express = require('express');
var router = express.Router();
var Driver = require('../app/models/driver');

router.route('/drivers')
    /**
     * GET call for the driver entity (multiple).
     * @returns {object} A list of drivers. (200 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     */
    .get(function (req, res) {
        Driver.find(function (err, drivers) {
            if (err) {
                res.status(500).json({ "status code": 500, "error code": "1002", "error message": "Internal server error" });
            } else {
                res.status(200).json(drivers);
            }
        });
    })
    /**
     * POST call for the driver entity.
     * @param {string} firstName - The first name of the new driver
     * @param {string} lastName - The last name of the new driver
     * @param {String} emailAddress - The emailAddress of the new driver
     * @param {string} password - The password of the new driver
     * @param {string} addressLine1 - The address line 1 of the new driver
     * @param {string} addressLine2 - The address line 2 of the new driver
     * @param {string} city - The city of the new driver
     * @param {string} state - The state of the new driver
     * @param {number} zip - The zip code of the new driver
     * @param {String} phoneNumber - The phone number of the new driver
     * @param {String} drivingLicense - The driver license of the new driver
     * @param {String} licensedState - The licensed State of the new driver
     * @returns {object} A message and the driver created. (201 Status Code)
     * @throws Bad Request (400 Status Code)
     */
    .post(function (req, res) {
        var driver = new Driver();
        driver.firstName = req.body.firstName;
        driver.lastName = req.body.lastName;
        driver.emailAddress = req.body.emailAddress;
        driver.password = req.body.password;
        driver.addressLine1 = req.body.addressLine1;
        driver.addressLine2 = req.body.addressLine2;
        driver.city = req.body.city;
        driver.state = req.body.state;
        driver.zip = req.body.zip;
        driver.phoneNumber = req.body.phoneNumber;
        driver.drivingLicense = req.body.drivingLicense;
        driver.licensedState = req.body.licensedState;
        driver.save(function (err) {
            if (err) {
                res.status(400).send(String(err));
            } else {
                res.status(201).json(driver);
            }
        });
    });

/** 
 * Express Route: /drivers/:driver_id
 * @param {string} driver_id - Id Hash of driver Object
 */
router.route('/drivers/:driver_id')
    /**
     * GET call for the driver entity (single).
     * @returns {object} the driver with Id driver_id. (200 Status Code)
     * @throws Not Found (404 Status Code)
     */
    .get(function (req, res) {
        Driver.findById(req.params.driver_id, function (err, driver) {
            if (err) {
                res.status(404).json({ "status code": 404, "error code": "1004", "error message": "Given driver does not exist" });
            } else {
                if (driver) {
                    res.status(200).json(driver);
                }
                else {
                    res.status(404).send(err);
                }
            }
        });
    })
    /**
     * PATCH call for the driver entity (single).
     * @param {string} firstName - The first name of the new driver
     * @param {string} lastName - The last name of the new driver
     * @param {String} emailAddress - The emailAddress of the new driver
     * @param {string} password - The password of the new driver
     * @param {string} addressLine1 - The address line 1 of the new driver
     * @param {string} addressLine2 - The address line 2 of the new driver
     * @param {string} city - The city of the new driver
     * @param {string} state - The state of the new driver
     * @param {number} zip - The zip code of the new driver
     * @param {String} phoneNumber - The phone number of the new driver
     * @param {String} drivingLicense - The driver license of the new driver
     * @param {String} licensedState - The licensed State of the new driver
     * @returns {object} A message and the driver updated. (200 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     * @throws Bad Request (400 Status Code)
     */
    .patch(function (req, res) {
        Driver.findById(req.params.driver_id, function (err, car) {
            if (err) {
                res.status(500).json({ "status code": 500, "error code": "1005", "error message": "The driver cannot be updated" });
            } else {
                driver.firstName = req.body.firstName;
                driver.lastName = req.body.lastName;
                driver.emailAddress = req.body.emailAddress;
                driver.password = req.body.password;
                driver.addressLine1 = req.body.addressLine1;
                driver.addressLine2 = req.body.addressLine2;
                driver.city = req.body.city;
                driver.state = req.body.state;
                driver.zip = req.body.zip;
                driver.phoneNumber = req.body.phoneNumber;
                driver.drivingLicense = req.body.drivingLicense;
                driver.licensedState = req.body.licensedState;
                driver.save(function (err) {
                    if (err) {
                        res.status(400).send(String(err));
                    } else {
                        res.status(200).json({ "message": "Driver Updated", "driverUpdated": driver });
                    }
                });
            }
        });
    })
    /**
     * DELETE call for the driver entity (single).
     * @returns {object} A string message. (200 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     */
    .delete(function (req, res) {
        Driver.remove({
            _id: req.params.driver_id
        }, function (err, driver) {
            if (err) {
                res.status(500).json({ "status code": 500, "error code": "1006", "error message": "The driver cannot be deleted" });
            } else {
                res.status(200).json({ "message": "Driver Deleted" });
            }
        });
    });

module.exports = router;