/** 
 * Express Route: /paymentaccounts
 * @author Clark Jeria, David Sung
 * @version 0.0.3
 */
var express = require('express');
var router = express.Router();
var PaymentAccount = require('../app/models/paymentaccount');

router.route('/paymentaccounts')
    /**
     * GET call for the paymentAccount entity (multiple).
     * @returns {object} A list of paymentAccounts. (200 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     */
    .get(function (req, res) {
        PaymentAccount.find(function (err, paymentAccounts) {
            if (err) {
                res.status(500).json({ "status code": 500, "error code": "1002", "error message": "Internal server error" });
            } else {
                res.status(200).json(paymentAccounts);
            }
        });
    })
    /**
     * POST call for the paymentAccount entity.
     * @param {string} accountType - The account type of the new paymentAccount
     * @param {integer} accountNumber - The account number of the new paymentAccount
     * @param {date} expirationDate - The expiration date of the new paymentAccount
     * @param {string} nameOnAccount - The name on account of the new paymentAccount
     * @param {string} bank - The bank of the new paymentAccount
     * @returns {object} A message and the paymentAccount created. (201 Status Code)
     * @throws Bad Request (400 Status Code)
     */
    .post(function (req, res) {
        var paymentAccount = new PaymentAccount();
        paymentAccount.accountType = req.body.accountType;
        paymentAccount.accountNumber = req.body.accountNumber;
        paymentAccount.expirationDate = req.body.expirationDate;
        paymentAccount.nameOnAccount = req.body.nameOnAccount;
        paymentAccount.bank = req.body.bank;
        paymentAccount.save(function (err) {
            if (err) {
                res.status(400).send(String(err));
            } else {
                res.status(201).json(paymentAccount);
            }
        });
    });

/** 
 * Express Route: /paymentaccounts/:paymentaccount_id
 * @param {string} paymentaccount_id - Id Hash of PaymentAccount Object
 */
router.route('/paymentaccounts/:paymentaccount_id')
    /**
     * GET call for the paymentAccount entity (single).
     * @returns {object} the paymentAccount with Id paymentaccount_id. (200 Status Code)
     * @throws Not Found (404 Status Code)
     */
    .get(function (req, res) {
        PaymentAccount.findById(req.params.paymentaccount_id, function (err, paymentAccount) {
            if (err) {
                res.status(404).json({ "status code": 404, "error code": "1004", "error message": "Given account does not exist" });
            } else {
                if (paymentAccount) {
                    res.status(200).json(paymentAccount)
                }
                else {
                    res.status(404).send(err);
                }
            }
        });
    })
    /**
     * PATCH call for the paymentAccount entity (single).
     * @param {string} accountType - The account type of the new paymentAccount
     * @param {integer} accountNumber - The account number of the new paymentAccount
     * @param {date} expirationDate - The expiration date of the new paymentAccount
     * @param {string} nameOnAccount - The name on account of the new paymentAccount
     * @param {string} bank - The bank of the new paymentAccount
     * @returns {object} A message and the paymentAccount updated. (200 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     * @throws Bad Request (400 Status Code)
     */
    .patch(function (req, res) {
        PaymentAccount.findById(req.params.paymentaccount_id, function (err, car) {
            if (err) {
                res.status(500).json({ "status code": 500, "error code": "1005", "error message": "The passenger cannot be updated" });
            } else {
                paymentAccount.accountType = req.body.accountType;
                paymentAccount.accountNumber = req.body.accountNumber;
                paymentAccount.expirationDate = req.body.expirationDate;
                paymentAccount.nameOnAccount = req.body.nameOnAccount;
                paymentAccount.bank = req.body.bank;
                paymentaccount.save(function (err) {
                    if (err) {
                        res.status(400).send(String(err));
                    } else {
                        res.status(200).json({ "message": "PaymentAccount Updated", "paymentAccountUpdated": paymentAccount });
                    }
                });
            }
        });
    })
    /**
     * DELETE call for the paymentaccount entity (single).
     * @returns {object} A string message. (200 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     */
    .delete(function (req, res) {
        PaymentAccount.remove({
            _id: req.params.paymentaccount_id
        }, function (err, paymentaccount) {
            if (err) {
                res.status(500).json({ "status code": 500, "error code": "1006", "error message": "The passenger cannot be deleted" });
            } else {
                res.status(200).json({ "message": "PaymentAccount Deleted" });
            }
        });
    });

module.exports = router;