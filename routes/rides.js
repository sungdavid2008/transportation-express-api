/** 
 * Express Route: /rides
 * @author Clark Jeria, David sung
 * @version 0.0.3
 */
var express = require('express');
var router = express.Router();
var Ride = require('../app/models/ride');

router.route('/rides')
    /**
     * GET call for the ride entity (multiple).
     * @returns {object} A list of rides. (200 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     */
    .get(function (req, res) {
        Ride.find(function (err, rides) {
            if (err) {
                res.status(500).json({ "status code": 500, "error code": "1002", "error message": "Internal server error" });

            } else {
                res.status(200).json(rides);
            }
        });
    })
    /**
     * POST call for the ride entity.
     * @param {string} rideType - The rideType of the new ride
     * @param {number} startPoint - The startPoint of the new ride
     * @param {number} endPoint - The endPoint of the new ride
     * @param {number} requestTime - The requestTime of the new ride
     * @param {number} pickupTime - The pickupTime of the new ride
     * @param {number} dropOffTime - The dropOffTime of the new ride
     * @param {string} status - The status of the new ride
     * @param {number} fare - The fare of the new ride
     * @returns {object} A message and the ride created. (201 Status Code)
     * @throws Bad Request (400 Status Code)
     */
    .post(function (req, res) {
        var ride = new Ride();
        ride.rideType = req.body.rideType;
        ride.startPoint = req.body.startPoint;
        ride.endPoint = req.body.endPoint;
        ride.requestTime = req.body.requestTime;
        ride.pickupTime = req.body.pickupTime;
        ride.dropOffTime = req.body.dropOffTime;
        ride.status = req.body.status;
        ride.fare = req.body.fare;

        ride.save(function (err) {
            if (err) {
                res.status(400).send(String(err));
            } else {
                res.status(201).json(ride);
            }
        });
    });

/** 
 * Express Route: /rides/:ride_id
 * @param {string} ride_id - Id Hash of Ride Object
 */
router.route('/rides/:ride_id')
    /**
     * GET call for the ride entity (single).
     * @returns {object} the ride with Id ride_id. (200 Status Code)
     * @throws Not Found (404 Status Code)
     */
    .get(function (req, res) {
        Ride.findById(req.params.ride_id, function (err, ride) {
            if (err) {
                res.status(404).json({ "status code": 404, "error code": "1004", "error message": "Given ride does not exist" });
            } else {
                if (ride) {
                    res.status(200).json(ride)
                }
                else {
                    res.status(404).send(err);
                }
            }
        });
    })
    /**
     * PATCH call for the ride entity (single).
     * @param {string} rideType - The rideType of the new ride
     * @param {number} startPoint - The startPoint of the new ride
     * @param {number} endPoint - The endPoint of the new ride
     * @param {number} requestTime - The requestTime of the new ride
     * @param {number} pickupTime - The pickupTime of the new ride
     * @param {number} dropOffTime - The dropOffTime of the new ride
     * @param {string} status - The status of the new ride
     * @param {number} fare - The fare of the new ride
     * @returns {object} A message and the ride updated. (200 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     * @throws Bad Request (400 Status Code)
     */
    .patch(function (req, res) {
        Ride.findById(req.params.ride_id, function (err, ride) {
            if (err) {
                res.status(500).json({ "status code": 500, "error code": "1005", "error message": "The ride cannot be updated" });
            } else {
                ride.rideType = req.body.rideType;
                ride.startPoint = req.body.startPoint;
                ride.endPoint = req.body.endPoint;
                ride.requestTime = req.body.requestTime;
                ride.pickupTime = req.body.pickupTime;
                ride.dropOffTime = req.body.dropOffTime;
                ride.status = req.body.status;
                ride.fare = req.body.fare;
                ride.save(function (err) {
                    if (err) {
                        res.status(400).send(String(err));
                    } else {
                        res.status(200).json({ "message": "Ride Updated", "rideUpdated": ride });
                    }
                });
            }
        });
    })
    /**
     * DELETE call for the ride entity (single).
     * @returns {object} A string message. (200 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     */
    .delete(function (req, res) {
        Ride.remove({
            _id: req.params.ride_id
        }, function (err, ride) {
            if (err) {
                res.status(500).json({ "status code": 500, "error code": "1006", "error message": "The ride cannot be deleted" });
            } else {
                res.status(200).json({ "message": "Ride Deleted" });
            }
        });
    });

    /** 
    * Express Route: /rides/:ride_id/routePoints
    * @param {string} ride_id - Id Hash of Ride Object
    */

router.route('/rides/:ride_id/routePoints')
    /**
     * GET call for the ride points entity.
     * @returns {object} the ride with Id ride_id. (200 Status Code)
     * @throws Not Found (404 Status Code)
     */
    .get(function (req, res) {
        Ride.findById(req.params.ride_id, function (err, ride) {
            if (err) {
                res.status(404).json({ "status code": 404, "error code": "1004", "error message": "Given ride does not exist" });
            } else {

                res.status(200).json(ride.route);
            }
        });
    })
    /**
    * POST call for the route points entity.
    * @param {Number} lat - the latitude of route point
    * @param {Number} lon - the longtitude of route point
    * @returns {object} A message and the ride created. (201 Status Code)
    * @throws Not Found (404 Status Code)
    */

    .post(function (req, res) {
        Ride.findByIdAndUpdate(req.params.ride_id, { $set: { "route": { lat: req.body.lat, lon: req.body.lon } } }, { new: true }, function (err, ride) {
            if (err) {
                res.status(404).json({ "status code": 404, "error code": "1004", "error message": "Given ride does not exist" });
            } else {
                res.status(201).json({ "message": "Route Points created", "routePoints": ride.route });
            }
        });
    })
    /** 
    * Express Route: /rides/:ride_id/routePoints
    * @param {string} ride_id - Id Hash of Ride Object
    */
router.route('/rides/:ride_id/routePoints/current')
    /**
      * GET call for the current ride points entity.
      * @returns {object} the ride with Id ride_id. (200 Status Code)
      * @throws Not Found (404 Status Code)
      */
    .get(function (req, res) {
        Ride.findById(req.params.ride_id, function (err, ride) {
            if (err) {
                res.status(404).json({ "status code": 404, "error code": "1004", "error message": "Given ride does not exist" });
            } else {
                var current = ride.route.slice(-1);
                res.status(200).json(current);
            }
        });
    })
module.exports = router;