/** 
 * Express Route: /cars
 * @author Clark Jeria, David Sung
 * @version 0.0.3
 */
var express = require('express');
var router = express.Router();
var Car = require('../app/models/car');

router.route('/cars')
    /**
     * GET call for the car entity (multiple).
     * @returns {object} A list of cars. (200 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     */
    .get(function (req, res) {
        Car.find(function (err, cars) {
            if (err) {
                res.status(500).json({ "status code": 500, "error code": "1002", "error message": "Internal server error" });
            } else {
                res.status(200).json(cars);
            }
        });
    })
    /**
     * POST call for the car entity.
     * @param {string} license - The license plate of the new car
     * @param {integer} doorCount - The amount of doors of the new car
     * @param {string} make - The make of the new car
     * @param {string} model - The model of the new car
     * @returns {object} A message and the car created. (201 Status Code)
     * @throws Bad Request (400 Status Code)
     */
    .post(function (req, res) {

        var car = new Car();
        car.license = req.body.license;
        car.doorCount = req.body.doorCount;
        car.make = req.body.make;
        car.model = req.body.model;
        car.save(function (err) {
            if (err) {
                res.status(400).send(String(err));
            } else {
                res.status(201).json(car);//{"make": car.make, "message" : "Car Created", "carCreated" : car});
            }
        });
    });

/** 
 * Express Route: /cars/:car_id
 * @param {string} car_id - Id Hash of Car Object
 */
router.route('/cars/:car_id')
    /**
     * GET call for the car entity (single).
     * @returns {object} the car with Id car_id. (200 Status Code)
     * @throws Not Found (404 Status Code)
     */
    .get(function (req, res) {
        Car.findById(req.params.car_id, function (err, car) {
            if (err) {
                res.status(404).json({ "status code": 404, "error code": "1004", "error message": "Given car does not exist" });
            } else {
                if (car) {
                    res.status(200).json(car);
                }
                else {
                    res.status(404).send(err);
                }
            }
        });
    })
    /**
     * PATCH call for the car entity (single).
     * @param {string} license - The license plate of the new car
     * @param {integer} doorCount - The amount of doors of the new car
     * @param {string} make - The make of the new car
     * @param {string} model - The model of the new car
     * @returns {object} A message and the car updated. (200 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     * @throws Bad Request (400 Status Code)
     */
    .patch(function (req, res) {
        Car.findById(req.params.car_id, function (err, car) {
            if (err) {
                res.status(500).json({ "status code": 500, "error code": "1005", "error message": "The car cannot be updated" });
            } else {
                car.license = req.body.license;
                car.doorCount = req.body.doorCount;
                car.make = req.body.make;
                car.model = req.body.model;
                car.save(function (err) {
                    if (err) {
                        res.status(400).send(String(err));
                    } else {
                        res.status(200).json({ "message": "Car Updated", "carUpdated": car });
                    }
                });
            }
        });
    })
    /**
     * DELETE call for the car entity (single).
     * @returns {object} A string message. (200 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     */
    .delete(function (req, res) {
        Car.remove({
            _id: req.params.car_id
        }, function (err, car) {
            if (err) {
                res.status(500).json({ "status code": 500, "error code": "1006", "error message": "The car cannot be deleted" });
            } else {
                res.status(200).json({ "message": "Car Deleted" });
            }
        });
    });
module.exports = router;