/** 
 * Mongoose Schema for the Entity Car
 * @author Clark Jeria, David Sung
 * @version 0.0.3
 */

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var CarSchema   = new Schema({
    license: {type: String, required: true, maxlength: [10, 'The max string length is 10']},
    doorCount: {type: Number, required: true, min: [1, 'The numer should be between 1-8'] , max: [8, 'The numer should be between 1-8']},
    make: {type: String, required: true, maxlength: [18, 'The max string length is 18']},
    model: {type: String, required: true, maxlength: [18, 'The max string length is 18']},   
    driver: { type: Schema.Types.ObjectId, ref: 'Driver' }
});

module.exports = mongoose.model('Car', CarSchema);