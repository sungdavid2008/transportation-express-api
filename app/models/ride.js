/** 
 * Mongoose Schema for the Entity Ride
 * @author Clark Jeria, David Sung
 * @version 0.0.3
 */

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var RideSchema   = new Schema({
    passenger: { type: Schema.Types.ObjectId, ref: 'Passenger' },
    driver: { type: Schema.Types.ObjectId, ref: 'Driver' },
    car: { type: Schema.Types.ObjectId, ref: 'Car' },
    rideType: {type: String,required: true, enum:['ECONOMY','PREMIUM','EXECUTIVE']},
    startPoint: {type: Schema.Types.Mixed, required: true},
    endPoint: {type: Schema.Types.Mixed, required: true},
    requestTime: {type: Number, required: true},
    pickupTime: {type: Number, required: true},
    dropOffTime: {type: Number, required: true},
    status:{type: String, required: true, enum: ['REQUESTED','AWAITING_DRIVER','DRIVER_ASSIGNED','IN_PROGRESS','ARRIVED','CLOSED']},
    fare: {type: Number},
    route: [{lat:Number,lon: Number}],  
});

module.exports = mongoose.model('Ride', RideSchema);