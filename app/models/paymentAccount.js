/** 
 * Mongoose Schema for the Entity PaymentAccount
 * @author Clark Jeria, David Sung
 * @version 0.0.1
 */

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var PaymentAccountSchema   = new Schema({
    accountType: {type: String, required: true, maxlength: [18, 'The max string length is 18']},
    accountNumber: {type: Number,required: true, max: [18, 'The numer should be no more than 18 digits']},
    expirationDate: {type: Number,required: true},
    nameOnAccount: {type: String,required: true, maxlength: [18, 'The max string length is 18']},
    bank: {type: String, required: true, maxlength: [18, 'The max string length is 18']},
});

module.exports = mongoose.model('PaymentAccount', PaymentAccountSchema);