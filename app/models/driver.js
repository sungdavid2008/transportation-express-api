/** 
 * Mongoose Schema for the Entity Driver
 * @author Clark Jeria, David Sung
 * @version 0.0.3
 */

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;


var DriverSchema   = new Schema({
    firstName: {type: String, required: true, minlength: [1, 'The string length must be between 1-15'] , maxlength: [15, 'The string length must be between 1-15']}, 
    lastName: {type: String, required: true,minlength: [1, 'The string length must be between 1-15'], maxlength:[15, 'The string length must be between 1-15']},
    emailAddress: {type: String,required: true, match: /[a-zA-Z0-9_.]+\@[a-zA-Z](([a-zA-Z0-9-]+).)*/},
    password: {type: String, required: true, minlength: [8, 'The string length must be between 8-16'] , maxlength: [16, 'The string length must be between 8-16']},
    addressLine1: {type: String,required: true, maxlength: [50, 'The max string length is 50']},
    addressLine2: {type: String, maxlength: [50, 'The max string length is 50']},
    city: {type: String,required: true, maxlength: [50, 'The max string length is 50']},
    state: {type: String,required: true, maxlength: [2, 'The max string length is 2']},
    zip: {type: String,required: true, maxlength: [5, 'The max string length is 5']},
    phoneNumber: {type: String,required: true, match: /^\d{3}-\d{3}-\d{4}$/},
    drivingLicense: {type: String,required: true, minlength: [8, 'The string length must be between 8-16'] , maxlength: [16, 'The string length must be between 8-16']},
    licensedState: {type: String,required: true, maxlength: [2, 'The max string length is 2']},
});

module.exports = mongoose.model('Driver', DriverSchema);